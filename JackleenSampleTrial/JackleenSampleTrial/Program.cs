﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JackleenSampleTrial
{
    public class Program
    {
        static void Main(string[] args)
        {
            bool tryAgain = true;

            while (tryAgain)
            {
                try
                {
                    Console.WriteLine("Please enter your Integer List with , between each number in one line...");
                    string Line = Console.ReadLine();
                    var ListOfIntegers = Line.Split(',').Select(Int32.Parse).ToList();
                    var tuple = Simplify(ListOfIntegers);

                    if (tuple.Item1 != true)
                    {
                        Console.WriteLine(tuple.Item2);
                    }
                    else
                    {
                        Console.WriteLine(tuple.Item2);
                        Console.WriteLine("Successfully Reordered.");
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        public static Tuple<bool, string> Simplify(List<int> ListOfIntegers)
        {
            string ReorderedList = null;
            ListOfIntegers = ListOfIntegers.Distinct().ToList();

            try
            {
                for (int y = 0; y < ListOfIntegers.Count; y++)
                {
                    int min = 999;
                    int NumberOrder = 0;
                    int i = 0;
                    while (i < ListOfIntegers.Count)
                    {
                        if (ListOfIntegers[i] < min && ListOfIntegers[i] != -1)
                        {
                            min = ListOfIntegers[i];
                            NumberOrder = i;
                        }
                        i++;
                    }
                    ListOfIntegers[NumberOrder] = -1;
                    if (i == (ListOfIntegers.Count - 1))
                    {
                        ReorderedList += min.ToString();
                    }
                    else
                    {
                        ReorderedList += min.ToString() + ",";
                    }
                    //ReorderedList.Add(min);
                }

                return new Tuple<bool, string>(true, ReorderedList);
            }
            catch (Exception e)
            {
                return new Tuple<bool, string>(false, e.ToString());
            }
        }

    }
}
